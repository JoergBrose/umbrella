//
//  UserPreferencesObjC.m
//  Umbrella
//
//  Created by Jörg Brose on 8/31/15.
//  Copyright (c) 2015 BroseBros. All rights reserved.
//

#import "UserPreferencesObjC.h"

@implementation UserPreferencesObjC

static NSString *const metricPreferredKey = @"umbrellaMetricPreferred"; //key for save/load of metricPrefered in NSUserDefault
static NSString *const zipCodeKey = @"umbrellaZipCode"; //key for save/load of zipCode in NSUserDefault


//Simple save of user preferences in NSUserDefaults. Could be enhanced for more complex data storage
- (void)saveUserPreferences {
    
    //prepare standard user defaults
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    //saving metricPrefered to user defaults.
    [defaults setBool:_metricPrefered forKey:metricPreferredKey];
    
    //test if zipCode is set before saving it
    if (_zipCode) {
        
        //saving zip code to user defaults
        [defaults setInteger:_zipCode forKey:zipCodeKey];
        
    }
    else {
        
        //just to be sure remove any object for zipCode from user defaults if zip code is nil
        [defaults removeObjectForKey:zipCodeKey];
        
    }
    
    //call the delegate method to trigger a reload of the data
    [self.delegate userPreferencesWerChanged];
    
}

//Simple load of user preferences in NSUserDefaults
- (void)loadUserPreferences {
    
    //prepare standard user defaults
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    //read metricPrefered from user defaults. returns default NO if no user default for key
    _metricPrefered = [defaults boolForKey:metricPreferredKey];
    
    //read zipCode from user defaults. returns default 0 if no user default for key
    _zipCode = [defaults integerForKey:zipCodeKey];
    
}

@end
