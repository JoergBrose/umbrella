//
//  IconHandler.swift
//  Umbrella
//
//  Created by Jörg Brose on 8/31/15.
//  Copyright (c) 2015 BroseBros. All rights reserved.
//

import UIKit

class IconHandler: NSObject {
    
    //Caches for normal and highlighted icons so that we don't need to load them multiple times from the external source
    //(due to the asynchronous request it can be that an icon isn't chached in time for the second request)
    var iconCache = [String: UIImage]()
    var highlightedIconCache = [String: UIImage]()
    
    func setIconImage(imageView: UIImageView, iconName: String, highlighted: Bool, color: UIColor) {
        if highlighted { //highlighted version of icon is requested
            
            //if highlight load without caching
            
            if let icon = highlightedIconCache[iconName] {
                
                //highlighted Icon found in cache
                imageView.image = icon
                
                //tint the image with the color (we need to set the rendering mode to AlwaysTemplate for this to have an effect)
                imageView.image = imageView.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
                
                imageView.tintColor = color
                
            } else {
                
                //Icon wasn't found in cache, we have to load it from external source
                
                let iconRequest = nrd_weatherRequestForIcon(iconName, highlighted)
                
                let queue = NSOperationQueue()
                
                NSURLConnection.sendAsynchronousRequest(iconRequest, queue: queue) { (response, data, error) -> Void in
                    if error == nil {
                        
                        var icon = UIImage(data: data)
                        
                        //tint highlighted image
                        
                        imageView.image = icon
                        
                        //tint the image with the color (we need to set the rendering mode to AlwaysTemplate for this to have an effect)
                        
                        imageView.image = imageView.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
                        
                        imageView.tintColor = color
                        
                        //Put the icon into the cache
                        self.highlightedIconCache[iconName] = icon
                    }
                    
                    //TODO: Error Handling when external load fails, e.g. a placeholder icon could be loaded instead
                    
                }
            
            }
            
        } else if let icon = iconCache[iconName] {
            //icon found in chache
            imageView.image = icon
            
        }
            
        else {
            
            //Icon wasn't found in cache, we have to load it from external source
            
            let iconRequest = nrd_weatherRequestForIcon(iconName, highlighted)
            
            let queue = NSOperationQueue()
            
            NSURLConnection.sendAsynchronousRequest(iconRequest, queue: queue) { (response, data, error) -> Void in
                if error == nil {
                    
                    let icon = UIImage(data: data)
                    
                    imageView.image = icon
                    
                    //Put the icon into the cache
                    self.iconCache[iconName] = icon
                    
                }
            }
            
        }
        
        
    }
   
}
