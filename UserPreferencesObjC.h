//
//  UserPreferencesObjC.h
//  Umbrella
//
//  Created by Jörg Brose on 8/31/15.
//  Copyright (c) 2015 BroseBros. All rights reserved.
//

#import <Foundation/Foundation.h>

//delegate protocol user to trigger a reload of the weather data if the user preferences have been changed (TODO: Try partial reload of just UI if only metricPrefered changed)
@protocol UserPreferenceDelegate

-(void)userPreferencesWerChanged; //method called in delegate when changes were made to the preferences (most important for zip code)

@end

@interface UserPreferencesObjC : NSObject

@property BOOL metricPrefered; //preference for temperature value display: FALSE for metric/celsius, TRUE for english/

@property NSInteger zipCode; //last entered zip code, optional: can be nil (at first start of app)

@property (nonatomic, assign) id delegate; //delegate id

- (void)saveUserPreferences; //saving current set user preferences

- (void)loadUserPreferences; //load user preferences from NSUserDefaults

@end

