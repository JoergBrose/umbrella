//
//  UserPreferences.swift
//  Umbrella
//
//  Created by Jörg Brose on 8/31/15.
//  Copyright (c) 2015 BroseBros. All rights reserved.
//

import UIKit

class UserPreferences: NSObject {
    
    var metricPrefered: Bool = false //for temperature value: FALSE for metric/celsius, TRUE for english/fahrenheit
    var zipCode: Int? = nil //last entered zip code, optional: can be nil (at first start of app)
    
    let metricPreferedKey = "umbrellaMetricPrefered" //key for save/load of metricPrefered in NSUserDefault
    let zipCodeKey = "umbrellaZipCode" //key for save/load of zipCode in NSUserDefault
 
    
    //Simple save of user preferences in NSUserDefaults. Could be enhanced for more complex data storage
    func saveUserPreferences() {
        
        //prepare standard user defaults
        let defaults = NSUserDefaults.standardUserDefaults()
        
        //saving metricPrefered to user defaults.
        defaults.setObject(metricPrefered, forKey: metricPreferedKey)
        
        //test if zipCode is set before saving it
        if let currentZipCode = zipCode {
            
            //saving zip code to user defaults
            defaults.setObject(currentZipCode, forKey: zipCodeKey)
            
        } else {
            
            //just to be sure remove any object for zipCode from user defaults if zip code is nil
            defaults.removeObjectForKey(zipCodeKey)
            
        }
        
    }
    
    
    //Simple load of user preferences in NSUserDefaults
    func loadUserPreferences() {
        
        //prepare standard user defaults
        let defaults = NSUserDefaults.standardUserDefaults()
        
        //check if an user default value exists for metricPrefered. Yes: read it into the var. No: Standard value is false
        if let metricPreferedFound: AnyObject = defaults.objectForKey(metricPreferedKey) {
            
            self.metricPrefered = defaults.boolForKey(metricPreferedKey)
            
        } else {
            
            self.metricPrefered = false
            
        }
        
        //check if an user default value exists for zip code. Yes: read it into the var. No: Set to Nil
        if let metricPreferedFound: AnyObject = defaults.objectForKey(zipCodeKey) {
            
            self.zipCode = defaults.integerForKey(zipCodeKey)
            
        } else {
            
            self.zipCode = nil
            
        }
        
    }
    
   
}
