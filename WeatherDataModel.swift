//
//  WeatherDataModel.swift
//  Umbrella
//
//  Created by Jörg Brose on 8/30/15.
//  Copyright (c) 2015 BroseBros. All rights reserved.
//

import UIKit

class WeatherDataModel: NSObject {
    
    //variables for current weather
    var currentTemperatureEnglish = "- °" //current temperature in fahrenheit/english
    var currentTemperatureMetric = "- °" //current temperature in celsius/metric
    var location = "loading location" //location (city and state for the provided zip code)
    var currentWeatherCondition = "" //current weather condition (e.g. cloudy, sunny etc.)
    var currentWeatherIconName = "" //name of the icon for the current weather condition (can differ from weather condition name)
    var weatherColor: UIColor = UIColor(red:0.01, green:0.66, blue:0.96, alpha:1.0) //warm or cold color for app background . Default is for cold weather
    
    //two dimensional arrays for hourly weather prediction (1st dimension day index, 2nd dimension hour/prediction index)
    var hourlyTime = Array<Array<String>>() //time for prediction, e.g. 9:00AM
    var hourlyIconName = Array<Array<String>>() //icon name for prediction, e.g. 'clear'
    var hourlyTemperatureMetric = Array<Array<Int>>() //temperature of prediction in celsius (metric)
    var hourlyTemperatureEnglish = Array<Array<Int>>() //temperature of prediction in fahrenheit (english)
    
    //view collection section info
    var numberOfPredictionsPerDay: [Int] = [] //array for number of predictions for each day index (sections in collection view)
    var nameOfDay: [String] = [] //name for the day index (today, tomorrow, thursday etc.)
    
    //high and low temperature of each day
    var highTempIndexOfDay: [Int] = [] //index of the prediction with the highest temperature of each day
    var lowTempIndexOfDay: [Int] = [] //index of the prediction with the lowest temperature of each day
    
    //helper variables for weather prediction json analysis
    var highTemperatureForProcessedDay: Int? = nil //high temperature up til now for the processed predicitions of the currently processed day. Can be nil if no prediction was processed yet for this day
    var lowTemperatureForProcessedDay: Int? = nil //low temperature up til now for the processed predicitions of the currently processed day. Can be nil if no prediction was processed yet for this day
    var processedDay: NSString = "" //name of currently being processed day
    var processedDayIndex = 0

    //colors for warm or cold background
    let warmColor = UIColor(red:1.00, green:0.60, blue:0.00, alpha:1.0)
    let coldColor = UIColor(red:0.01, green:0.66, blue:0.96, alpha:1.0)
    
    //reads the important information of the json into the weather model variables. Sends back true if successful, false if something went wrong
    func setDataFromJSON(weatherJSON: NSDictionary) -> Bool {
        
        //empty all arrays and reset all helper variables
        hourlyTime = []
        hourlyIconName = []
        hourlyTemperatureMetric = []
        hourlyTemperatureEnglish = []
        numberOfPredictionsPerDay = []
        
        nameOfDay = []
        highTempIndexOfDay = []
        lowTempIndexOfDay = []
        
        highTemperatureForProcessedDay = nil
        lowTemperatureForProcessedDay = nil
        processedDay = ""
        processedDayIndex = 0
        
        
        //analyze JSON
        //get the current observation
        if let currentObservation = weatherJSON["current_observation"] as? NSDictionary {
            
            //get the current temperature in fahrenheit/english
            if let temperature = currentObservation["dewpoint_f"] as? NSNumber {
                
                currentTemperatureEnglish = "\(temperature)°"
                
                //Check for needed UI color (cold color when under 60, warm color when equal or higher)
                if Int(temperature) < 60 {
                    
                    weatherColor = coldColor
                    
                }
                
                else {
                    
                    weatherColor = warmColor
                    
                }
                
            } else {return false} //error: json could not be read
            
            //get the current temperature in celsius/metric
            if let temperature = currentObservation["dewpoint_c"] as? NSNumber {
                
                currentTemperatureMetric = "\(temperature)°"
                
            } else {return false} //error: json could not be read
            
            //get the location for the provided zip (City, State)
            if let locationDictionary = currentObservation["display_location"] as? NSDictionary {
                
                if let fullLocation = locationDictionary["full"] as? NSString {
                    
                    location = "\(fullLocation)"
                    
                }
                
            } else {return false} //error: json could not be read
            
            //get the current weather condition
            if let weatherCondition = currentObservation["weather"] as? NSString {
                
                currentWeatherCondition = "\(weatherCondition)"
                
            } else {return false} //error: json could not be read
            
            //get the icon name for the current weather condition
            if let iconName = currentObservation["icon"] as? NSString {
                
                currentWeatherIconName = "\(iconName)"
                
            } else {return false} //error: json could not be read
            
        }  else {return false} //error: json could not be read
        
        //read the hourly forecast section
        if let hourlyForecast = weatherJSON["hourly_forecast"] as? NSArray {
            
            //read data for each hourly prediction
            for var i = 0; i < hourlyForecast.count; i++ {
                
                //the forecast json
                let newForecast = hourlyForecast[i] as! NSDictionary
                
                //read time data from forecast (name of day and time)
                if let forecastTime = newForecast["FCTTIME"] as? NSDictionary {
                    
                    //read the name of the day
                    if let forecastDay = forecastTime["weekday_name_unlang"] as? NSString {
                        
                        //if this day is still the currently processed day we have one more prediction for that day
                        if forecastDay == processedDay {
                                
                            numberOfPredictionsPerDay[processedDayIndex] += 1
                            
                            
                        } else {
                           
                            //new day begins
                            
                            //set day name and prepare new number of items for this day
                            processedDay = forecastDay
                            numberOfPredictionsPerDay.append(1)
                            
                            //if the twodimensional arrays are still empty, no day was processed yet
                            if hourlyTime.count == 0 {
                                
                                //not necessary, but to improve readability
                                processedDayIndex = 0
                                
                            } else {
                                
                                // else day index has to be incremented
                                processedDayIndex++
                                
                            }
                            
                            
                            
                            //append new arrays to all twodimensional arrays representing the new day
                            hourlyTime.append([])
                            hourlyIconName.append([])
                            hourlyTemperatureMetric.append([])
                            hourlyTemperatureEnglish.append([])
                            
                            //reset helper vars for low/high temp for new day
                            highTemperatureForProcessedDay = nil
                            lowTemperatureForProcessedDay = nil
                            
                            //put a placeholder object representing the high and low temps for this day into the array for better accessibility
                            highTempIndexOfDay.append(0)
                            lowTempIndexOfDay.append(0)
                            
                            //name of the day
                            //if this is the first day, it's today
                            if numberOfPredictionsPerDay.count == 1 {
                                
                                nameOfDay.append("Today")
                            
                            //if it's the second day, it's tomorrow
                            } else if numberOfPredictionsPerDay.count == 2 {
                                
                                nameOfDay.append("Tomorrow")
                                
                            //else take the weekday name
                            } else {
                                
                                nameOfDay.append("\(forecastDay)")
                                
                            }
                            
                        }
                        
                        
                    }  else {return false} //error: json could not be read
                    
                    //read time for the prediction, e.g. 9:00AM
                    if let forecastHour = forecastTime["civil"] as? NSString {
                        
                        hourlyTime[processedDayIndex].append("\(forecastHour)")
                        
                    }
                    
                }  else {return false} //error: json could not be read
                
                //read name of forecast weather condition icon
                if let forecastIconName = newForecast["icon"] as? NSString {
                    
                    hourlyIconName[processedDayIndex].append("\(forecastIconName)")
                    
                }  else {return false} //error: json could not be read
                
                //read forecast temperature object
                if let forecastTemperature = newForecast["dewpoint"] as? NSDictionary {
                    
                    //read the forecast temperature in celsius
                    if let forecastTemperatureMetric = forecastTemperature["metric"] as? NSString {
                        
                        hourlyTemperatureMetric[processedDayIndex].append(forecastTemperatureMetric.integerValue)
                        
                    } else {return false} //error: json could not be read
                    
                    //read the forecast temperature in fahrenheit and test for high/low temperature
                    if let forecastTemperatureEnglish = forecastTemperature["english"] as? NSString {
                        
                        hourlyTemperatureEnglish[processedDayIndex].append(forecastTemperatureEnglish.integerValue)
                        
                        //Check for new daily highest temperature
                        
                        //if no high was yet recorded for this day, it's automatically the new high
                        if highTemperatureForProcessedDay == nil {
                            
                            //set the new highest temperature
                            highTemperatureForProcessedDay = forecastTemperatureEnglish.integerValue
                            
                            //remember the index of this prediction for the current day (-1 because array starts at 0 and we count the predictions starting with 1)
                            highTempIndexOfDay[processedDayIndex] = numberOfPredictionsPerDay[processedDayIndex] - 1
                            
                        //else if it's higher than the current high temperature, it's the new high
                        } else if highTemperatureForProcessedDay < forecastTemperatureEnglish.integerValue {
                            
                            //set the current highest temperature
                            highTemperatureForProcessedDay = forecastTemperatureEnglish.integerValue
                            
                            //remember the index of this prediction for the current day (-1 because array starts at 0 and we count the predictions starting with 1)
                            highTempIndexOfDay[processedDayIndex] = numberOfPredictionsPerDay[processedDayIndex] - 1
                            
                        }
                        
                        //Check for new daily lowest temperature
                        
                        //if no low was yet recorded for this day, it's automatically the new low
                        if lowTemperatureForProcessedDay == nil {
                            
                            //set the new lowest temperature
                            lowTemperatureForProcessedDay = forecastTemperatureEnglish.integerValue
                            
                            //remember the index of this prediction for the current day (-1 because array starts at 0 and we count the predictions starting with 1)
                            lowTempIndexOfDay[processedDayIndex] = numberOfPredictionsPerDay[processedDayIndex] - 1
                            
                        } else if lowTemperatureForProcessedDay > forecastTemperatureEnglish.integerValue {
                            
                            //set the new lowest temperature
                            lowTemperatureForProcessedDay = forecastTemperatureEnglish.integerValue
                            
                            //remember the index of this prediction for the current day (-1 because array starts at 0 and we count the predictions starting with 1)
                            lowTempIndexOfDay[processedDayIndex] = numberOfPredictionsPerDay[processedDayIndex] - 1
                            
                        }
                        
                    }  else {return false} //error: json could not be read
                    
                        
                }  else {return false} //error: json could not be read
                
                
            }
            
        }  else {return false} //error: json could not be read
        
        return true
        
    }
    
    
}
