//
//  SettingsViewControllerObjCViewController.m
//  Umbrella
//
//  Created by Jörg Brose on 8/31/15.
//  Copyright (c) 2015 BroseBros. All rights reserved.
//

#import "SettingsViewControllerObjCViewController.h"

@interface SettingsViewControllerObjCViewController ()

@end

@implementation SettingsViewControllerObjCViewController

//tests if the zip code is valid (TODO: better test [online?]. Currently only checked if length is right)
-(BOOL)isFormDataValid{
    
    //reset the error message
    NSString *errorMessage = nil;
    
    UITextField *errorField; //UI Element for error display
    
    //test if zip code is empty
    if([_zipCodeTextField.text isEqualToString:@""])
    {
        //set error message
        errorMessage = @"Please enter a zip code";
        errorField = _zipCodeTextField;
    }
    //test if zip code is not 5 digits long
    else if([[_zipCodeTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]!=5)
    {
        //set error message
        errorMessage = @"Zip Code must be 5 digits long";
        errorField = _zipCodeTextField;
    }
    if (errorMessage) {
        //if error occured, show error message popup
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Invalid input" message:errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [errorField becomeFirstResponder];
        return NO;
    }else{
        return YES;
    }
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {

    //if a valid zip code is in user defaults, show it as default value
    if (_userPreferences.zipCode > 1) {
        
        _zipCodeTextField.text = [NSString stringWithFormat:@"%li", (long)_userPreferences.zipCode];
        
    }
    
    //set the selected segment of the scale control to the preferred temperature unit
    if (_userPreferences.metricPrefered) {
        
        _scaleControl.selectedSegmentIndex = 1;
        
    } else {
        
        _scaleControl.selectedSegmentIndex = 0;
        
    }
    
}
    


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

//Action when the 'get weather' button was pressed by user. Set's the user defaults with current UI data and dismisses the view if zip code is valid. if zip code is not valid, an error is displayed
- (IBAction)getWeatherButtonPressed:(id)sender {
    
    //check if zip code is valid
    if ([self isFormDataValid]) {
        
        //set the provided zip code to the user preference object
        _userPreferences.zipCode = [_zipCodeTextField.text integerValue];
        
        //read the segmented control (temperature display preference)
        NSString *tempSystemPreference = [_scaleControl titleForSegmentAtIndex:_scaleControl.selectedSegmentIndex];
        
        //set metricPrefered depending of segmented control
        if ([tempSystemPreference  isEqual: @"Celsius"]) {
            
            //set preferenece to metric/celsius
            _userPreferences.metricPrefered = YES;
            
        } else {

            //set preferenece to english/fahrenheit
            _userPreferences.metricPrefered = NO;
            
        }
        
        //trigger method to save preferences in prefence object (using NSUserDefaults)
        [_userPreferences saveUserPreferences];
        
        //dismiss modular settings view to get back to weather view
        [self dismissViewControllerAnimated:NO completion:nil];
        
    };
    
    
}
@end
