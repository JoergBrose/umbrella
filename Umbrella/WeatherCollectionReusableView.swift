//
//  WeatherCollectionReusableView.swift
//  Umbrella
//
//  Created by Jörg Brose on 8/30/15.
//  Copyright (c) 2015 BroseBros. All rights reserved.
//

import UIKit

class WeatherCollectionReusableView: UICollectionReusableView {
    
    
    @IBOutlet weak var dayLabel: UILabel!
    
}
