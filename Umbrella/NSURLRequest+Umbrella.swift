//
//  NSURLRequest+Umbrella.swift
//  Umbrella
//
//  Created by Jörg Brose on 8/30/15.
//  Copyright (c) 2015 BroseBros. All rights reserved.
//

import Foundation

func nrd_weatherRequestForIcon(icon: NSString, highlighted: Bool) -> NSURLRequest
{
    let baseURL = NSURL(string: "http://nerdery-umbrella.s3.amazonaws.com/")
    var selectedString = ""
    if highlighted {
        selectedString = "-selected"
    }
    if let iconURL = NSURL(string: "\(icon)\(selectedString).png", relativeToURL: baseURL) {
        return NSURLRequest(URL: iconURL)
    }
    else {
        return NSURLRequest()
    }
    //TODO: Error Handling

}