//
//  SettingsViewControllerObjCViewController.h
//  Umbrella
//
//  Created by Jörg Brose on 8/31/15.
//  Copyright (c) 2015 BroseBros. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UserPreferencesObjC.h"

@interface SettingsViewControllerObjCViewController : UIViewController

//Outlet to the text field for zip code entry
@property (weak, nonatomic) IBOutlet UITextField *zipCodeTextField;

//Outlet to the segmented control for the temperature display option (celsius/fahrenheit)
@property (weak, nonatomic) IBOutlet UISegmentedControl *scaleControl;

//userPreferences object. Should always be set to the WeatherViewControllers instance of the UserPreferences (done in segue preperation from WVC to SVC)
@property UserPreferencesObjC *userPreferences;

//object for error handling
@property NSError *error;

//Action when the 'get weather' button was pressed by user. Set's the user defaults with current UI data and dismisses the view if zip code is valid. if zip code is not valid, an error is displayed
- (IBAction)getWeatherButtonPressed:(id)sender;

//tests if the zip code is valid (TODO: better test [online?]. Currently only checked if length is right)
- (BOOL)isFormDataValid;

@end


