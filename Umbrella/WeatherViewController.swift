//
//  WeatherViewController.swift
//  Umbrella
//
//  Created by Jörg Brose on 8/30/15.
//  Copyright (c) 2015 BroseBros. All rights reserved.
//
/*
WeatherViewController handles the Weather view.
It is also the delegate and datasource of the collection view for the weather predictions
*/

import UIKit

class WeatherViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UserPreferenceDelegate {
    
    let reuseIdentifier = "WeatherCell" //reuse identifier for the cells (the hourly predictions of the weather prediction CollectionView
    let headerReuseIdentifier = "collectionHeader" // reuse identifier for the headers (days) of the CollectionView
    
    let weatherAPIKey = "c6af502cfe959d32" //The API key
    
    var weatherDataModel = WeatherDataModel() //The main data model. This is the source all weather-related data including the current weather and the hourly predictions
    
    var iconHandler = IconHandler() //the icon handler provides the icons for the weather predictions
    
    var userPreferences = UserPreferencesObjC() //user preferences: temperature display unit (fahrenheit/celsius) and current zip code
    
    @IBOutlet var weatherView: UIView! //outlet to the main weather UIView

    @IBOutlet weak var locationLabel: UILabel! //outlet to the label for location (city/state)
    
    @IBOutlet weak var currentTemperatureLabel: UILabel! //outlet to the label for the current temperature
    
    @IBOutlet weak var currentConditionLabel: UILabel! //outlet to the label for the current weather condition
    
    @IBOutlet weak var weatherCollectionView: UICollectionView! //Outlet to the weather collection view
    
    @IBOutlet weak var weatherCollectionViewHeader: UICollectionReusableView! // outlet to the weather collection header
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Load User preferences especially zip code and if metric system should be used (if avaiable
        userPreferences.loadUserPreferences();
        
        //set self as delegate for UserPreferencesObjC
        userPreferences.delegate = self
        
        //register for the notification when the app comes to the foreground
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("appDidBecomeActive"), name: UIApplicationDidBecomeActiveNotification, object: nil)
        
    }
    
    //method that is called when app comes to the foreground (registered via notification UIApplicationDidBecomeActiveNotification)
    func appDidBecomeActive() {
        
        if userPreferences.zipCode == 0 {
            // if no zipCode present we have to go to settings and let the user input a zip
            
            self.sendAlertPopupAndGoToSettingsView("No zip code found", message: "Please enter a zip code in the settings")
            
        } else {
            
            //if we have a zip, reload the data model
            updateDataModel()
            
        }
        
    }
    
    //goes to the seetings view with the segue
    func goToSettingsView() {
        
        self.performSegueWithIdentifier("toSettingsView", sender: self)
        
    }
    
    //gets the weather json from the web, propagates it to the weatherDataModel to build a new model and sets the current weather view components
    func updateDataModel() {
        
        //get the weather api client
        var weatherAPISharedClient = WeatherAPIClient.sharedClient()
        
        //set the api key
        weatherAPISharedClient.APIKey = weatherAPIKey
        
        //get the weather json
        weatherAPISharedClient.getForecastAndConditionsForZipCode( String(userPreferences.zipCode), withCompletionBlock: { (success, result, error) -> Void in
            
            if success {
                
                //give weather json to weatherDataModel for further processing
                let jsonReadSuccessful = self.weatherDataModel.setDataFromJSON(result as NSDictionary)
                
                //if json was processed successfully
                if jsonReadSuccessful {
                    
                    //set the current temperature label to the prefered temperature value
                    if self.userPreferences.metricPrefered {
                        
                        self.currentTemperatureLabel.text = self.weatherDataModel.currentTemperatureMetric
                        
                    } else {
                        
                        self.currentTemperatureLabel.text = self.weatherDataModel.currentTemperatureEnglish
                        
                    }
                    
                    //set values on weather view and backgroundcolor (warm or cold)
                    self.locationLabel.text = self.weatherDataModel.location
                    self.currentConditionLabel.text = self.weatherDataModel.currentWeatherCondition
                    self.weatherView.backgroundColor = self.weatherDataModel.weatherColor
                    
                    //reload the data of the weather prediction collection view
                    self.weatherCollectionView.reloadData()
                    
                } else {
                    
                    //if something went wrong with the processing of the json, alert the user and go to settings screen for new zip code input
                    self.sendAlertPopupAndGoToSettingsView("No valid result for zip code", message: "Please enter a new zip code in the settings")
                    
                }
            
            } else {
                
                //if something went wrong with the processing of the json, alert the user and go to settings screen for new zip code input
                self.sendAlertPopupAndGoToSettingsView("No result for zip code", message: "Please enter a new zip code in the settings")
                
            }
            
        })
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        //Return the number of sections/days
        return weatherDataModel.numberOfPredictionsPerDay.count //count of array is the number of sections/days
    }
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //Return the number of items (hourly predictions) in the section/day
        return weatherDataModel.numberOfPredictionsPerDay[section]
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        //load weather prediction for this cell/hour
        
        //prepare cell
        let cell: WeatherCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! WeatherCollectionViewCell
        
        //set the time of the prediction
        cell.timeLabel.text = weatherDataModel.hourlyTime[indexPath.section][indexPath.row]

        //Depending on the user preference we set the temperature in Celsius (metric) or Fahrenheit (English)
        if userPreferences.metricPrefered {

            cell.temperatureLabel.text = "\(weatherDataModel.hourlyTemperatureMetric[indexPath.section][indexPath.row])°"
            
        } else {
        
            cell.temperatureLabel.text = "\(weatherDataModel.hourlyTemperatureEnglish[indexPath.section][indexPath.row])°"
        
        }
            
        // Check if this was the highest or lowest temperature of the day (if it is both, they are cancelling each other)
        if (weatherDataModel.highTempIndexOfDay[indexPath.section] == indexPath.row)
            && !(weatherDataModel.lowTempIndexOfDay[indexPath.section] == indexPath.row) {
           //was highest temperature of the day
                
          //get highlighted icon from iconHandler with warm color
          iconHandler.setIconImage(cell.iconView, iconName: weatherDataModel.hourlyIconName[indexPath.section][indexPath.row], highlighted: true, color: weatherDataModel.warmColor)
        
          //set time label to warm color
          cell.timeLabel.textColor = weatherDataModel.warmColor
            
          //set temperature label to warm color
          cell.temperatureLabel.textColor = weatherDataModel.warmColor
            
            
        } else if (weatherDataModel.lowTempIndexOfDay[indexPath.section] == indexPath.row) 
                  && !((weatherDataModel.highTempIndexOfDay[indexPath.section] == indexPath.row)){
            //was lowest temperature of the day
                    
            //get highlighted icon from iconHandler with cold color
            iconHandler.setIconImage(cell.iconView, iconName: weatherDataModel.hourlyIconName[indexPath.section][indexPath.row], highlighted: true, color: weatherDataModel.coldColor)
             
             //set time label to cold color
            cell.timeLabel.textColor = weatherDataModel.coldColor
            
            //set temperature label to cold color
            cell.temperatureLabel.textColor = weatherDataModel.coldColor
                
        } else {
            //was neither highest nor lowest temperature (or both and they cancelled each other)
            
            //get icon from iconHandler
            iconHandler.setIconImage(cell.iconView, iconName: weatherDataModel.hourlyIconName[indexPath.section][indexPath.row], highlighted: false, color: UIColor.blackColor())
            
            //set time label to black color (not really necessary, just to make sure)
            cell.timeLabel.textColor = UIColor.blackColor()
            
            //set temperature label to black color (not really necessary, just to make sure)
            cell.temperatureLabel.textColor = UIColor.blackColor()
        }
        
        //return the finished weather prediction cell
        return cell
    }
    
    //method for adding the header lines in the weather prediction collection view (with the name of the day)
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        
        //prepare header
        var header: WeatherCollectionReusableView = WeatherCollectionReusableView()
        
        //checkt if it is a section header
        if kind == UICollectionElementKindSectionHeader {
            
            //get the supplementary view
            header = (collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: headerReuseIdentifier, forIndexPath: indexPath) as? WeatherCollectionReusableView)!
            
            //set the name of the day
            header.dayLabel.text = weatherDataModel.nameOfDay[indexPath.section]
            
        }
        
        //return the finished header line
        return header
        
    }
    
    //preparation of segue
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        //check if the segue is the segue to the settings view
        if segue.identifier == "toSettingsView" {
        
            //prepare the settings view controller
            let destinationVC = segue.destinationViewController as! SettingsViewControllerObjCViewController
        
            //propagate the userPreferences object
            destinationVC.userPreferences = userPreferences
            
        }
        
        
    }
    
    //delegate method from UserPreferences
    func userPreferencesWerChanged() {
        
        //if user preferences have changed, we need to update the data model
        updateDataModel()
        
    }
    
    //create and present an alert popup and go to the settingsview after it is closed
    func sendAlertPopupAndGoToSettingsView(title: String, message: String) {
        
        //create the alertController
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertControllerStyle.Alert)
        
        //add action to dismiss the alert and register the handler for going to the settings view
        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default,
            handler: {(alert: UIAlertAction!) in self.goToSettingsView()}))
        
        //present the alert popup
        self.presentViewController(alertController, animated: true, completion: nil)
        
    }


}

